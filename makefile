TARGET = ray-marching
SOURCES = src/*

$(TARGET): $(SOURCES)
	clang++ src/$(TARGET).cpp -Wall -Werror -lGL -lGLEW -lglfw -o $(TARGET)