
#pragma once

#include "includes.cpp"

ostream& operator<<(ostream& stream, const vec3& v) {
    return stream << v.x << " " << v.y << " " << v.z;
}

string fileToStr(const string& path) {
	string result;
	ifstream fileStream(path);

	if(fileStream.is_open()){
		stringstream stringStream;
		stringStream << fileStream.rdbuf();
		result = stringStream.str();
		fileStream.close();
	}else{
        cout << "Impossible to open : " << path << endl;
	}

    return result;
}

bool strEndsWith(const string& s, const string& e) {
	int p = s.size() - e.size();

	if (p < 0) {
		cout << s << " can not ends with " << e << endl;
		return false;
	}

	for (int i = 0; i < e.size(); i++) {
		if (s[p + i] != e[i]) {
			return false;
		}
	}

	return true;
}