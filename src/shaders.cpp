#pragma once

#include "includes.cpp"
#include "utils.cpp"

uint createShader(string path) {
    uint type;

         if (strEndsWith(path, ".vert")) type = GL_VERTEX_SHADER;
    else if (strEndsWith(path, ".frag")) type = GL_FRAGMENT_SHADER;
    else if (strEndsWith(path, ".tesc")) type = GL_TESS_CONTROL_SHADER;
    else if (strEndsWith(path, ".tese")) type = GL_TESS_EVALUATION_SHADER;
    else if (strEndsWith(path, ".geom")) type = GL_GEOMETRY_SHADER;
    else if (strEndsWith(path, ".comp")) type = GL_COMPUTE_SHADER;
    else cout << "ERROR Unknow shader type : " << path << endl;

    uint shader = glCreateShader(type);
    string source = fileToStr(path);
    const char* source_c_str = source.c_str();
    glShaderSource(shader, 1, &source_c_str, nullptr);
    glCompileShader(shader);
    return shader;
}

uint createProgram(vector<string> paths) {
    uint program = glCreateProgram();
    vector<uint> shaders;

    for (string path : paths) {
        uint shader = createShader(path);
        shaders.push_back(shader);
        glAttachShader(program, shader);
    }

    glLinkProgram(program);
    glUseProgram(program);

    for (uint shader : shaders) {
        glDetachShader(program, shader);
        glDeleteShader(shader);
    }

    return program;
}