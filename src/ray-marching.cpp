#include "includes.cpp"
#include "utils.cpp"
#include "sdf.cpp"
#include "gl-context.cpp"
#include "camera.cpp"
#include "mesh.cpp"

#include "omgl.cpp"

int main(int argc, char** argv) {
    // parse arguments

    if (argc != 4) {
        cerr << "usage : " << argv[0] << " mesh.stl width mode" << endl;
        exit(1);
    }

    string meshFilename = argv[1];
    int width = atoi(argv[2]);
    bool analyzeMode = argv[3] == string("analyze");

    // create window and opengl context

    GLFWwindow* window = initGL(1000, 1000, "Ray Marching Demo");

    uint vao; // what does a vao do ? don't ask me
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);

    // load model

    Mesh mesh(meshFilename);
    SDF sdf(mesh, width);

    // create opengl objects

    float screenQuadVertices[] = {
        -1, -1,    1, -1,    1,  1,
         1,  1,   -1,  1,   -1, -1,
    };

    uint screenQuadBuffer;
    glGenBuffers(1, &screenQuadBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, screenQuadBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(screenQuadVertices), screenQuadVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);

    if (analyzeMode) {
        omgl::program analyzeProgram({ "shaders/screen-quad.vert", "shaders/analyse.frag" });

        int layerMin = 0;
        int layerMax = sdf.getSize().z-1;
        int layer = (layerMin + layerMax)/2;

        // main loop

        while (glfwWindowShouldClose(window) == false) {
            // process inputs

            glfwPollEvents();

            if (glfwGetKey(window, GLFW_KEY_UP) && layer < layerMax) {
                layer++;
            }

            if (glfwGetKey(window, GLFW_KEY_DOWN) && layer > layerMin) {
                layer--;
            }

            cout << "\rViewing SDF layer : " << layer+1 << "/" << layerMax+1 << "   " << flush;

            // use quadrant views

            ivec2 windowSize;
            glfwGetWindowSize(window, &windowSize.x, &windowSize.y);
            ivec2 quadrantSize = windowSize / 2;

            glClear(GL_COLOR_BUFFER_BIT);
            analyzeProgram.uniform("windowSize", quadrantSize);
            analyzeProgram.uniform("layer", layer);

            sdf.getDistanceTexture().unit(0);
            sdf.getSignTexture().unit(1);
            sdf.getSdfTexture().unit(2);

            // distance

            glViewport(0, 0, quadrantSize.x, quadrantSize.y);
            analyzeProgram.uniform("mode", 0);
            glDrawArrays(GL_TRIANGLES, 0, 6);

            // sign
            
            glViewport(quadrantSize.x, quadrantSize.y, quadrantSize.x, quadrantSize.y);
            analyzeProgram.uniform("mode", 1);
            glDrawArrays(GL_TRIANGLES, 0, 6);

            // sdf

            glViewport(quadrantSize.x, 0, quadrantSize.x, quadrantSize.y);
            analyzeProgram.uniform("mode", 2);
            glDrawArrays(GL_TRIANGLES, 0, 6);

            // swap buffers

            glfwSwapBuffers(window);
        }

        cout << endl;
    } else {
        omgl::program renderProgram({ "shaders/screen-quad.vert", "shaders/ray-marching.frag" });
        Camera camera(window);

        // main loop

        while (glfwWindowShouldClose(window) == false) {
            camera.update();

            ivec2 windowSize;
            glfwGetWindowSize(window, &windowSize.x, &windowSize.y);
            glViewport(0, 0, windowSize.x, windowSize.y);
            glClear(GL_COLOR_BUFFER_BIT);

            renderProgram.uniform("windowSize", windowSize);
            renderProgram.uniform("sdfMat", sdf.getTransform());
            renderProgram.uniform("cameraMat", camera.getTransform());
            renderProgram.uniform("fov", camera.getFov());

            sdf.getSdfTexture().bind();
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glfwSwapBuffers(window);
        }
    }

    // cleanup

    terminateGL();

    return 0;
}